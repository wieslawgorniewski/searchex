# README

## Ruby version is 2.5.1. Using RVM/rbenv is highly recommended.

OSX: https://gorails.com/setup/osx/10.13-high-sierra

Ubuntu: https://gorails.com/setup/ubuntu/18.10

https://rvm.io/

https://rvm.io/gemsets

https://github.com/rbenv/rbenv


## Install yarn

OSX: `brew install yarn`

Ubuntu: `sudo apt-get install yarn`


## Install requirements:

`bundle install --without production`

`gem install bundler`

`yarn install`

## Compile front-end code

ONE time:  

`./bin/webpack`

OR start the watcher in another terminal:  

`./bin/webpack --watch --colors --progress`

## Migrate and Seed DB

`rm db/test.sqlite3`

`bundle exec rake db:migrate RAILS_ENV=test`

`rm db/development.sqlite3`

`bundle exec rake db:migrate`

`bundle exec rake db:seed`

## Start server

`bundle exec rails s`  

App will be running under: http://localhost:3000/  

# TESTS

## Running tests

`bundle exec rspec`

## Checking coverage

Simplecov produces nice HTML coverage in `/coverage/index.html` located in the root project tree.
