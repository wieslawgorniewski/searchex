import { Route, withRouter } from 'react-router-dom';
import React from 'react';

import ROUTES from './routes';
import Home from './home';


class Body extends React.Component {
  render() {
    return (
      <div id="body">
        <Route path={ROUTES.home.spec} exact render={(props) => (<Home />)}/>
      </div>
    );
  }
}

const BodyWithRoute = withRouter(props => <Body {...props}/>);

export default BodyWithRoute;
