import Route from 'route-parser';

const ROUTES = {
  'home': new Route('/'),
}

export default ROUTES;
