import React from 'react';

class Home extends React.Component {
  constructor() {
    super();

    this.queryString = require('query-string');
    this.state = {
      models: ['tickets'],
      definitions: {
        'tickets': []
      },
      model: 'tickets',
      field_name: 'tickets.id',
      search_term: '',
      results: []
    }
  }

  componentWillMount () {
    fetch(
      '/api/definitions/'
    ).then(
      (response) => {
        response.json().then((data) => {
          this.setState(
            (state) => ({
              definitions: data,
              models: Object.keys(data)
            })
          )
        });
      }
    ).catch((err) => {});
  }

  componentDidMount () {
    const parsed = this.queryString.parse(window.location.search);
    let field_name = parsed['field_name'] || 'tickets.id';
    let search_term = parsed['search_term'] || '';
    let model = parsed["model"] || 'tickets';
    this.setState(
      (state) => (
        {
          field_name: field_name,
          search_term: search_term,
          model: model
        }
      )
    )
    fetch(
      `/api/${model}/${window.location.search}`
    ).then(
      (response) => {
        if (response.status == 200) {
          response.json().then((data) => {
            this.setState(
              (state) => (
                {
                  results: data
                }
              )
            )
          });
        }
      }
    ).catch((err) => {});
  }

  render() {
    const RadioField = (data) => {
      let field = data.field;
      if (field) {
        let fieldArray = field.split('.');
        let fieldModel = fieldArray[0];
        let fieldName = (fieldModel == this.state.model) ? fieldArray[1] : field;

        return <label className="pointer">
          <input className="uk-radio uk-margin-small-right"
                 type="radio"
                 name="field_name"
                 value={field}
                 defaultChecked={this.state.field_name == field}></input>
            {fieldName}
        </label>
      }
      return ''
    }

    const RadioModel = (data) => {
      let model = data.model;
      if (model) {
        return <label className="pointer">
          <input className="uk-radio uk-margin-small-right"
                 type="radio"
                 name="model"
                 value={model}
                 onChange={() => {this.setState((state) => ({model: model, field_name: `${model}.id`, results: []}))}}
                 defaultChecked={this.state.model == model}></input>
            {model}
        </label>
      }
      return ''
    }

    const SearchBox = () => {
      return <div id="searchBox" className="uk-width-1-1@m uk-card uk-card-default uk-card-body uk-margin-small-top uk-margin-small-bottom border-blue">
        <form method="GET">
            <fieldset className="uk-fieldset">
                <h3>Searchex</h3>
                <div className="uk-margin uk-grid uk-child-width-auto@m">
                  <span>
                    Search in:
                  </span>
                  {
                    this.state.models.map(
                      (el, elIndex) => <RadioModel key={elIndex} model={el} />
                    )
                  }
                </div>
                <div>
                  <div className="uk-width-1-2@m uk-float-left">
                      <input className="uk-input"
                             type="text"
                             name="search_term"
                             placeholder="Search"
                             defaultValue={this.state.search_term}></input>
                  </div>
                  <div className="uk-width-1-3@m uk-float-left">
                      <button id="searchButton" className="uk-button uk-button-primary">
                        Search
                      </button>
                  </div>
                </div>
                <div className="uk-clearfix"></div>
                <div className="uk-margin uk-grid uk-child-width-1-4@m">
                  {
                    definitionsForModel().map(
                      (el, elIndex) => <RadioField key={elIndex} field={el} />
                    )
                  }
                </div>
            </fieldset>
        </form>
      </div>
    }

    const Result = (data) => {
      let result = data.result;
      return <div className="uk-card uk-card-default uk-card-body uk-margin-small-bottom">
          <h3 className="uk-card-title">ID: {result.id}</h3>
          <ul className="uk-list uk-list-bullet">
            {
              Object.keys(result).map(
                (key, elIndex) =>  {
                  let value = (Array.isArray(result[key])) ? result[key].join(', ') : result[key];
                  value = (value === null || value === undefined) ? '' : value.toString();
                  return <li key={elIndex}><strong>{key}:</strong> {value}</li>;
                }
              )
            }
          </ul>
      </div>
    }

    const Results = () => {
      return <div className="uk-width-1-1@m uk-padding-remove-left">
        {
          this.state.results.map(
            (el, elIndex) => <Result key={elIndex} result={el} />
          )
        }
      </div>
    }

    const definitionsForModel = () => {
      return this.state.definitions[this.state.model] || [];
    }

    return (
      <div className="uk-margin-small-top">
        <a className="uk-button uk-button-secondary uk-button-small uk-position-absolute uk-position-top-left" onClick={() => {window.location = '/'}}>
          Reset
        </a>
        <div className="uk-grid">
          <SearchBox />
          <Results />
        </div>
      </div>
    )
  }
}

export default Home;
