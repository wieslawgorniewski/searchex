class SearchInternal
  attr_accessor :errors

  def initialize
    @errors = []
  end

  def get_results(model, fields, search_term, options)
    if check_for_errors(model, fields).length > 0
      return []
    end
    query = build_query(fields, search_term, options)
    model = build_relations(model)
    model.where(query).distinct
  end

  private
  def sanitize_columns(model, fields)
    wrong_columns = Set.new
    model.searchable_relations.each do |m|
      wrong_columns.add(fields.reject { |f| m.searchable_fields.include?(f) })
    end
    wrong_columns = wrong_columns.to_a
    if wrong_columns.all? { |e| e.length > 0}
      return wrong_columns
    end
    []
  end

  def check_for_errors(model, fields)
    wrong_columns = sanitize_columns(model, fields)
    if wrong_columns.length > 0
      error = "Given columns are not related to entity #{model}: #{wrong_columns.join(', ')}."
      @errors.push(error)
    end
    @errors
  end

  def build_query(fields, search_term, options)
    query = ['']
    operator = (options[:strategy] == 'in_all') ? 'AND' : 'OR'
    fields.each_with_index do |field, index|
      operator = (index > 0) ? " #{operator} " : ''
      comparison = (search_term == '') ? 'IS NULL' : '= ?'
      query[0] += "#{operator}#{field} #{comparison}"
      query.push("#{search_term}")
    end
    query
  end

  def build_relations(model)
    model.searchable_relations.each do |m|
      unless model == m
        model = model.includes(m.model_name.param_key.pluralize)
      end
      if REFERENCED_MODELS.include?(m.to_s)
        model = model.references(m.model_name.param_key.pluralize)
      end
    end
    model
  end
end
