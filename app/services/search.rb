class Search
  attr_accessor :errors
  STRATEGIES = ['in_all', 'in_any']
  DEFAULT_OPTIONS = {
    strategy: 'in_any'
  }
  def initialize(model)
    @errors = []
    @model = model
    @service = nil
    case SEARCH_SERVICE_ENGINE
    when 'ransack'
      @service = SearchRansnack.new
    else
      @service = SearchInternal.new
    end
  end

  def get_results(params, options=DEFAULT_OPTIONS)
    params = params.stringify_keys
    fields = params['field_name'] || ["#{@model.table_name}.id"]
    fields = if fields.kind_of?(Array) then fields else [fields] end
    search_term = params['search_term'] || ''
    results = @service.get_results(@model, fields, search_term, options)
    @errors = @service.errors
    results
  end
end
