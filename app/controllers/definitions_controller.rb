class DefinitionsController < ApplicationController
  def index
    models = {}
    models['organizations'] = get_fields_for_model(Organization)
    models['users'] = get_fields_for_model(User)
    models['tickets'] = get_fields_for_model(Ticket)
    render json: models
  end

  private
  def get_fields_for_model(model)
    result = []
    model.searchable_relations.each do |m|
      result.push(m.searchable_fields)
    end
    result.flatten!
  end
end
