class ApplicationController < ActionController::Base
  def get_results
    @search_service = Search.new(controller_name.classify.constantize)
    @results = @search_service.get_results(params)
  end

  def render_errors
    if @search_service.errors.length > 0
      return render json: @search_service.errors, status: 422
    end
  end
end
