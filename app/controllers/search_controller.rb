class SearchController < ApplicationController
  before_action :get_results, only: [:index]
  before_action :render_errors, only: [:index]

  def index
    render json: @results
  end
end
