class Domain < ApplicationRecord
  belongs_to :organization
  BLACKLIST_SEARCHABLE_FIELDS = ['id', 'created_at', 'updated_at', 'organization_id']

  def self.searchable_relations
    [self]
  end
end
