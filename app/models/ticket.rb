class Ticket < ApplicationRecord
  belongs_to :organization
  belongs_to :assignee, class_name: 'User'
  belongs_to :submitter, class_name: 'User'
  acts_as_taggable
  validates :external_id, uniqueness: true

  def self.searchable_relations
    [self, Tag]
  end
end
