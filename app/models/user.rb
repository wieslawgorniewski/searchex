class User < ApplicationRecord
  belongs_to :organization
  acts_as_taggable
  validates :external_id, uniqueness: true

  def self.searchable_relations
    [self, Tag]
  end
end
