class Tag < ActsAsTaggableOn::Tag
  def self.searchable_relations
    [self]
  end

  def self.searchable_fields
    ["#{self.table_name}.name"]
  end
end
