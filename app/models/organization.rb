class Organization < ApplicationRecord
  has_many :users
  has_many :domains
  acts_as_taggable
  validates :external_id, uniqueness: true

  def self.searchable_relations
    [self, Tag, Domain]
  end
end
