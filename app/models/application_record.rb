class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  BLACKLIST_SEARCHABLE_FIELDS = []

  def self.searchable_fields
    column_names = self.column_names.reject { |c| self::BLACKLIST_SEARCHABLE_FIELDS.include?(c)}
    column_names.map { |c|"#{self.table_name}.#{c}" }
  end
end
