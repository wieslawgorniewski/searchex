class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.integer :_id
      t.string :url
      t.string :external_id
      t.string :event_type
      t.string :subject
      t.string :description
      t.string :priority
      t.string :status
      t.references :organization, foreign_key: true
      t.boolean :has_incidents
      t.datetime :due_at
      t.string :via

      t.timestamps
    end
  end
end
