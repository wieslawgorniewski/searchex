# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

fixtures_path = Rails.root.join('spec', 'fixtures')


# ADD ORGANIZATIONS

organizations = JSON.parse(File.read("#{fixtures_path}/organizations.json"))
known_organizations__ids = Organization.all.pluck(:_id)

organizations.each do |o|
  unless known_organizations__ids.include?(o['_id'])
    domain_names = o.delete('domain_names')
    tags = o.delete('tags')

    organization = Organization.create(o)
    organization.tag_list = tags
    organization.save
    domain_names.each do |domain_name|
      Domain.create(name: domain_name, organization: organization)
    end
  end
end

# ADD USERS

users = JSON.parse(File.read("#{fixtures_path}/users.json"))
known_users__ids = User.all.pluck(:_id)

users.each do |u|
  unless known_users__ids.include?(u['_id'])
    tags = u.delete('tags')
    organization = Organization.find_by(_id: u.delete('organization_id'))

    user = User.create(u)
    user.organization = organization
    user.tag_list = tags
    user.save
  end
end

# ADD TICKETS

tickets = JSON.parse(File.read("#{fixtures_path}/tickets.json"))
known_tickets__ids = Ticket.all.pluck(:_id)

tickets.each do |t|
  unless known_tickets__ids.include?(t['_id'])
    tags = t.delete('tags')
    event_type = t.delete('type')
    organization = Organization.find_by(_id: t.delete('organization_id'))
    assignee = User.find_by(_id: t.delete('assignee_id'))
    submitter = User.find_by(_id: t.delete('submitter_id'))

    ticket = Ticket.create(t)
    ticket.organization = organization
    ticket.assignee = assignee
    ticket.submitter = submitter
    ticket.tag_list = tags
    ticket.event_type = event_type
    ticket.save
  end
end
