require 'rails_helper'

RSpec.describe Search do
  describe 'initialize' do
    it 'uses internal search service by default' do
      expect(SearchInternal).to receive(:new)
      Search.new(Ticket)
    end

    it 'uses ransack if given in initializers' do
      stub_const('SEARCH_SERVICE_ENGINE', 'ransack')
      expect(SearchRansnack).to receive(:new)
      Search.new(Ticket)
    end

    it 'has no errors when initialized' do
      service = Search.new(Ticket)
      expect(service.errors).to eq([])
    end
  end

  describe 'get_results' do
    it 'passes correct arguments to used service get_results method inside' do
      params = {
        'field_name': ['tickets.status', 'tickets.id'],
        'search_term': 'pending'
      }
      expect_any_instance_of(SearchInternal).to receive(
        :get_results
      ).with(Ticket, ['tickets.status', 'tickets.id'], 'pending', Search::DEFAULT_OPTIONS)
      service = Search.new(Ticket)
      service.get_results(params)
    end

    it 'changes fields from string to array with one entry if needed' do
      params = {
        'field_name': 'tickets.status',
        'search_term': 'pending'
      }
      expect_any_instance_of(SearchInternal).to receive(
        :get_results
      ).with(Ticket, ['tickets.status'], 'pending', Search::DEFAULT_OPTIONS)
      service = Search.new(Ticket)
      service.get_results(params)
    end

    it 'sets search_term to empty string if not given' do
      params = {
        'field_name': 'tickets.status'
      }
      expect_any_instance_of(SearchInternal).to receive(
        :get_results
      ).with(Ticket, ['tickets.status'], '', Search::DEFAULT_OPTIONS)
      service = Search.new(Ticket)
      service.get_results(params)
    end

    it 'sets field_name to ["id"] if not given' do
      params = {
        'search_term': 'pending'
      }
      expect_any_instance_of(SearchInternal).to receive(
        :get_results
      ).with(Ticket, ['tickets.id'], 'pending', Search::DEFAULT_OPTIONS)
      service = Search.new(Ticket)
      service.get_results(params)
    end

    it 'assigns errors if userd service has any' do
      params = {
        'search_term': 'pending',
        'field_name': 'tickets.aaa'
      }
      service = Search.new(Ticket)
      service.get_results(params)
      expect(service.errors).to eq(['Given columns are not related to entity Ticket: tickets.aaa.'])
    end

    it 'returns results if any' do
      params = {
        'search_term': 'pending',
        'field_name': 'tickets.status'
      }
      service = Search.new(Ticket)
      result = service.get_results(params)
      expect(result.count).to eq(Ticket.where(status: 'pending').count)
    end
  end
end
