require 'rails_helper'

RSpec.describe SearchInternal do
  before :all do
    @service = SearchInternal.new
  end

  describe 'get_results' do
    context 'correct calls with known column names' do
      it 'gives results that match Organization.details exactly' do
        result = @service.get_results(Organization, ['organizations.details'], 'MegaCorp', {})
        expect(result.count).to eq(Organization.where(details: 'MegaCorp').count)
      end

      it 'gives results that match User.active exactly' do
        result = @service.get_results(User, ['users.active'], '0', {})
        expect(result.count).to eq(User.where(active: false).count)
      end

      it 'gives results that match Ticket.submitter_id exactly' do
        result = @service.get_results(Ticket, ['tickets.submitter_id'], '9', {})
        expect(result.count).to eq(Ticket.where(submitter_id: 9).count)
      end

      it 'gives results that match Tag.name exactly' do
        result = @service.get_results(Tag, ['tags.name'], 'Melton', {})
        expect(result.count).to eq(Tag.where(name: 'Melton').count)
      end

      it 'gives results that match Domain.name exactly' do
        result = @service.get_results(Domain, ['domains.name'], 'valreda.com', {})
        expect(result.count).to eq(Domain.where(name: 'valreda.com').count)
      end

      it 'searches for associated Tags if given in fields param' do
        result = @service.get_results(Ticket, ['tags.name'], 'South Carolina', {})
        expect(result.count).to eq(Ticket.tagged_with("South Carolina").count)
      end

      it 'searches for associated one to many (like Domain) if given in fields param' do
        result = @service.get_results(Organization, ['domains.name'], 'radiantix.com', {})
        expect(result.count).to eq(Organization.includes(:domains).where(domains: {name: 'radiantix.com'}).count)
      end

      it 'returns results if field is nil and search term is empty' do
        result = @service.get_results(Organization, ['organizations.details'], '', {})
        expect(result.count).to eq(Organization.where(details: nil).count)
      end

      it 'does not return result if search term does not meet field value exactly' do
        result = @service.get_results(Organization, ['organizations.details'], 'MegaCor', {})
        expect(result.count).to eq(0)
      end

      it 'is case sensitive during search' do
        result = @service.get_results(Organization, ['organizations.details'], 'megacorp', {})
        expect(result.count).to eq(0)
      end
    end

    context 'incorrect calls with unknown column names' do
      it 'assigns errors with wrong column names for the model' do
        result = @service.get_results(Organization, ['tickets._id'], 'MegaCorp', {})
        expect(@service.errors).to eq(
          ['Given columns are not related to entity Organization: tickets._id.']
        )
      end

      it 'returns empty array as a result when wrong column names for the model' do
        result = @service.get_results(Organization, ['organizations.zzz'], 'MegaCorp', {})
        expect(result).to eq([])
      end
    end
  end
end
