require 'rails_helper'

describe DefinitionsController, type: :controller do
  it 'returns 200 with field names data' do
    get :index
    expect(response.status).to eq(200)
    expect(JSON.parse(response.body)['organizations'][0]).to eq('organizations.id')
    expect(JSON.parse(response.body)['users'][0]).to eq('users.id')
    expect(JSON.parse(response.body)['tickets'][0]).to eq('tickets.id')
  end
end
