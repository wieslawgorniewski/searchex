require 'rails_helper'

describe OrganizationsController, type: :controller do
  it 'returns 200 with data as json list when it exists' do
    get :index, params: {
      search_term: 'comvex.com',
      field_name: ['domains.name']
    }
    expect(response.status).to eq(200)
    expect(JSON.parse(response.body).length).to eq(
      Organization.joins(:domains).where(domains: {name: 'comvex.com'}).count
    )
  end

  it 'returns 200 with empty list when it does not exists' do
    get :index, params: {
      search_term: 'nutralab',
      field_name: ['organizations.name']
    }
    expect(response.status).to eq(200)
    expect(JSON.parse(response.body)).to eq([])
  end

  it 'returns 422 and error list when request is wrong' do
    get :index, params: {
      search_term: 'nutralab',
      field_name: ['gdfg']
    }
    expect(response.status).to eq(422)
    expect(JSON.parse(response.body)).to eq(['Given columns are not related to entity Organization: gdfg.'])
  end
end
