require 'rails_helper'

describe TicketsController, type: :controller do
  it 'returns 200 with data as json list when it exists' do
    get :index, params: {
      search_term: 'pending',
      field_name: ['tickets.status']
    }
    expect(response.status).to eq(200)
    expect(JSON.parse(response.body).length).to eq(Ticket.where(status: 'pending').count)
    expect(JSON.parse(response.body)[0]).to eq(
      JSON.parse(Ticket.where(status: 'pending').first.to_json)
    )
  end

  it 'returns 200 with empty list when it does not exists' do
    get :index, params: {
      search_term: 'pendingaa',
      field_name: ['tickets.status']
    }
    expect(response.status).to eq(200)
    expect(JSON.parse(response.body)).to eq([])
  end

  it 'returns 422 and error list when request is wrong' do
    get :index, params: {
      search_term: 'pending',
      field_name: ['organizations.details']
    }
    expect(response.status).to eq(422)
    expect(JSON.parse(response.body)).to eq(['Given columns are not related to entity Ticket: organizations.details.'])
  end
end
