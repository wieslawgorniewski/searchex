require 'rails_helper'

describe UsersController, type: :controller do
  it 'returns 200 with data as json list when it exists' do
    get :index, params: {
      search_term: 'Sri Lanka',
      field_name: ['users.timezone']
    }
    expect(response.status).to eq(200)
    expect(
      JSON.parse(response.body)
    ).to eq(
      [
        JSON.parse(User.where(timezone: 'Sri Lanka').last.to_json)
      ]
    )
  end

  it 'returns 200 with empty list when it does not exists' do
    get :index, params: {
      search_term: 'Sri Lankaaaa',
      field_name: ['users.timezone']
    }
    expect(response.status).to eq(200)
    expect(JSON.parse(response.body)).to eq([])
  end

  it 'returns 422 and error list when request is wrong' do
    get :index, params: {
      search_term: 'Sri Lanka',
      field_name: ['users.timezonea']
    }
    expect(response.status).to eq(422)
    expect(JSON.parse(response.body)).to eq(['Given columns are not related to entity User: users.timezonea.'])
  end
end
