require 'rails_helper'

RSpec.describe ApplicationRecord, type: :model do
  describe 'self.searchable_fields' do
    it 'returns column names together with table name' do
      expect(User.searchable_fields.length).to eq(User.column_names.length)
      expect(User.searchable_fields[0]).to eq("#{User.table_name}.id")
    end

    it 'does not return column names that are blackisted' do
      stub_const('User::BLACKLIST_SEARCHABLE_FIELDS', ['id'])
      expect(User.searchable_fields.length).not_to eq(User.column_names.length)
      expect(User.searchable_fields[0]).not_to eq("#{User.table_name}.id")
      expect(User.searchable_fields.length).to eq(User.column_names.length-1)
    end
  end
end
